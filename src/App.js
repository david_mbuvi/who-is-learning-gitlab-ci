import React from 'react';
import logo from './gitlab.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <a
          className="App-link"
          href="https://gitlab.com"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn GitLab CI
        </a>
      </header>
      <div className="participants">
        <h3>
          Add your name to the list to demonstrate you have completed the course.
        </h3>
        <table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Username</th>
              <th>Location</th>
              <th>Message</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Valentin</td>
              <td>@vdespa</td>
              <td>Romania 🇷🇴</td>
              <td>Thank you for taking this course.</td>
            </tr>
            <tr>
              <td>Phat NV</td>
              <td>@phat-nv</td>
              <td>Viet Nam</td>
              <td>Thank you for sharing this course.</td>
            </tr>
            <tr>
              <td>Rahul Gupta</td>
              <td>@rahulgupta141998</td>
              <td>India</td>
              <td>Thank you for the course</td>
            </tr>
            <tr>
              <td>Santosh Dawanse</td>
              <td>@dawanse-santosh</td>
              <td>Nepal 🇳🇵</td>
              <td>Great content, Thank you for offering this course 💙.</td>
            </tr>
            <tr>
              <td>Maciej Friedel</td>
              <td>@ippolit</td>
              <td>Poland</td>
              <td>Thanks to this course I had a successful weekend</td>
            </tr>
            <tr>
              <td>Mohamed Nasrullah</td>
              <td>@nasr16</td>
              <td>India</td>
              <td>Really great course. Learned a lot in this one video. Thank you for offering this course 💙.</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default App;
